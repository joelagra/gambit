package gambitMap.countdowns;

import gambitMap.mainFolder.MainApp;
import gambitMap.util.ChatHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class InvaderCountdown extends Countdown {
    private static final int COUNTDOWN_TIME = 30;

    private final MainApp mainApp;
    private final Player player;
    private final Location[] spawnPositions;

    private int seconds;
    private boolean running;

    public InvaderCountdown(MainApp mainApp, Player player, Location[] spawnPositions) {
        this.mainApp = mainApp;
        this.player = player;
        this.spawnPositions = spawnPositions;
        seconds = COUNTDOWN_TIME;
    }

    /**
     * Starts the countdown and send messages to the teleported player
     */
    @Override
    public void start() {
        // starts the countdown
        running = true;
        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(mainApp, () -> {
            // send message for specific seconds and stop on 0
            switch (seconds){
                case 30: case 20: case 10: case 5: case 4: case 3: case 2:
                    ChatHandler.sendMessage("§aDu hast noch §6" + seconds + " Sekunden§a!", player);
                    break;
                case 1:
                    ChatHandler.sendMessage("§aDu hast noch §6eine Sekunde§a!", player);
                    break;
                case 0:
                    ChatHandler.sendMessage("§aDu wirst zurück teleportiert!", player);
                    stop();
                    break;
                default:
                    break;
            }
            seconds--;
        }, 0, 20);
    }

    /**
     * Stop the countdown and teleports player back
     */
    @Override
    public void stop() {
        if(running) {
            // stops the countdown
            Bukkit.getScheduler().cancelTask(taskID);

            // reset the countdown
            running = false;
            seconds = COUNTDOWN_TIME;

            // teleport the player back to his side
            player.teleport(spawnPositions[new Random().nextInt(spawnPositions.length)]);
        }
    }

    public boolean isRunning() {
        return running;
    }
}
