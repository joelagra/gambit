package gambitMap.commands;

import gambitMap.enums.Area;
import gambitMap.enums.PortalPositions;
import gambitMap.mainFolder.MainApp;
import gambitMap.map.Portal;
import gambitMap.util.ChatHandler;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PortalSetupCommand implements CommandExecutor, Listener, TabCompleter {
    public static String AXE_NAME = "§aMagic Staff";

    private final MainApp mainApp;
    private final HashMap<String, PortalPositions> position = new HashMap<>();
    private final HashMap<String, Portal> selectedPortal = new HashMap<>();

    public PortalSetupCommand(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("Gambit.portalSetup")) {
                if (args.length > 0) {
                    if (args[0].equalsIgnoreCase("create")) {
                        if (args.length == 2) {
                            // create the portal
                            createPortal(args[1], player);
                        } else {
                            ChatHandler.sendMessage("§Bitte nutze §6/setup <CREATE> <TEAM_AREA_1, TEAM_AREA_2>", player);
                        }
                    } else if (args[0].equalsIgnoreCase("set")) {
                        if (args.length == 3) {
                            // set the position
                            setPosition(args[1], args[2], player);
                        } else {
                            ChatHandler.sendMessage("§cBitte nutze §6/setup <SET> <TEAM_AREA_1, TEAM_AREA_2> <POSITION_1, POSITION_2, MIDDLE>", player);
                        }
                    } else if (args[0].equalsIgnoreCase("animate")) {
                        if (args.length == 3) {
                            // animate the portal
                            try {
                                int duration = Integer.parseInt(args[2]);
                                animatePortal(args[1], duration, player);
                            } catch (NumberFormatException e) {
                                ChatHandler.sendMessage("§cBitte nutze §6/setup <ANIMATE> <TEAM_AREA_1, TEAM_AREA_2>> <DURATION>", player);
                            }
                        } else {
                            ChatHandler.sendMessage("§cBitte nutze §6/setup <ANIMATE> <TEAM_AREA_1, TEAM_AREA_2>> <DURATION>", player);
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> tabs = new ArrayList<>();
        if (args.length == 1) {
            tabs.add("create");
            tabs.add("set");
            tabs.add("animate");
        } else if (args.length == 2 && (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("animate"))) {
            tabs.add("team_area_1");
            tabs.add("team_area_2");
        } else if (args.length == 3 && args[0].equalsIgnoreCase("set")) {
            tabs.add("middle");
            tabs.add("position_1");
            tabs.add("position_2");
        }
        return tabs;
    }

    private void createPortal(String areaStr, Player player) {
        Area area = null;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // check if area is set
        if (area != null) {
            // create portal and if successful send message
            Portal portal = new Portal(mainApp, player.getWorld(), area);
            if (portal.create()) {
                ChatHandler.sendMessage("§aDas Portal wurde erstellt!", player);
            } else {
                ChatHandler.sendMessage("§cDas Portal existiert bereits!", player);
            }
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/setup <CREATE> <TEAM_AREA_1, TEAM_AREA_2>", player);
        }
    }

    private void setPosition(String areaStr, String position, Player player) {
        Area area = null;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // check if area is set
        if (area != null) {
            // initialize portal and check if exists
            Portal portal = new Portal(mainApp, player.getWorld(), area);
            if (portal.exists()) {
                // check the selected position and set the portal
                if (position.equalsIgnoreCase(PortalPositions.POSITION_1.toString())) {
                    setPortal(player, portal, PortalPositions.POSITION_1);
                } else if (position.equalsIgnoreCase(PortalPositions.POSITION_2.toString())) {
                    setPortal(player, portal, PortalPositions.POSITION_2);
                } else if (position.equalsIgnoreCase(PortalPositions.MIDDLE.toString())) {
                    setPortal(player, portal, PortalPositions.MIDDLE);
                } else {
                    ChatHandler.sendMessage("§cBitte nutze §6/setup <SET> <TEAM_AREA_1, TEAM_AREA_2> <POSITION_1, POSITION_2, MIDDLE>", player);
                }
            } else {
                ChatHandler.sendMessage("§cDas Portal muss erst erstellt werden!", player);
            }
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/setup <SET> <TEAM_AREA_1, TEAM_AREA_2> <POSITION_1, POSITION_2, MIDDLE>", player);
        }
    }

    private void setPortal(Player player, Portal portal, PortalPositions pos) {
        // create item axe
        ItemStack axe = new ItemStack(Material.IRON_AXE);
        ItemMeta axeItemMeta = axe.getItemMeta();
        if (axeItemMeta != null) {
            axeItemMeta.setDisplayName(AXE_NAME);
        }
        axe.setItemMeta(axeItemMeta);

        // add the axe if not in inventory
        if (!player.getInventory().contains(axe)) {
            player.getInventory().addItem(axe);
        }

        // put the portal and the position in arrays
        selectedPortal.put(player.getName(), portal);
        position.put(player.getName(), pos);

        ChatHandler.sendMessage("§aJetzt links klicke mit der Axt auf einen Block!", player);
    }

    private void animatePortal(String areaStr, int duration, Player player) {
        Area area = null;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // check if area is set
        if (area != null) {
            // initialize portal and check if exists
            Portal portal = new Portal(mainApp, player.getWorld(), area);
            if (portal.exists()) {
                // load positions and animate the portal
                portal.load();
                if (!portal.animate(duration)) {
                    ChatHandler.sendMessage("§cDie Positionen 1, 2 und die Middle Position müssen gesetzt werden!", player);
                }
            } else {
                ChatHandler.sendMessage("§cDas Portal muss erst erstellt werden!", player);
            }
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/setup <ANIMATE> <TEAM_AREA_1, TEAM_AREA_2> <DURATION>", player);
        }
    }

    @EventHandler
    public void onAxeHit(PlayerInteractEvent event) {
        // check if it was a left click on a block
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
            return;
        }

        // get current player and the item in hand
        Player player = event.getPlayer();
        ItemMeta itemMeta = player.getInventory().getItemInMainHand().getItemMeta();

        // check if item meta is set
        if (itemMeta == null) {
            return;
        }

        // check if it is the real axe
        if (!itemMeta.getDisplayName().equals(AXE_NAME)) {
            return;
        }

        // cancel the event
        event.setCancelled(true);

        // get the clicked block and if block is set
        Block block = event.getClickedBlock();
        if (block == null) {
            return;
        }

        // get the location of the block
        Location location = block.getLocation();

        // check if the player is in the arrays
        if (selectedPortal.containsKey(player.getName()) && position.containsKey(player.getName())) {
            // remove the portal
            Portal portal = selectedPortal.remove(player.getName());

            // remove the position, send message accordingly to the position and set the location
            switch (position.remove(player.getName())) {
                case POSITION_1:
                    ChatHandler.sendMessage("§aDie Position 1 wurde erfolgreich gesetzt!", player);
                    portal.setStartPosition(location);
                    break;

                case POSITION_2:
                    ChatHandler.sendMessage("§aDie Position 2 wurde erfolgreich gesetzt!", player);
                    portal.setEndPosition(location);
                    break;

                case MIDDLE:
                    ChatHandler.sendMessage("§aDie Position der Mitte wurde erfolgreich gesetzt!", player);
                    portal.setMiddlePosition(location);
                    break;
            }
            // save the position
            portal.save();
        }
    }
}