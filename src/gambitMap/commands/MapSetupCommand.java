package gambitMap.commands;

import gambitMap.mainFolder.MainApp;
import gambitMap.map.Map;
import gambitMap.map.TeamArea;
import gambitMap.util.ChatHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class MapSetupCommand implements CommandExecutor, TabCompleter {
    private final MainApp mainApp;

    public MapSetupCommand(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("Gambit.mapSetup")) {
                if (args.length > 0) {
                    if (args[0].equalsIgnoreCase("create")) {
                        Map map = new Map(mainApp, player.getWorld());
                        if (!map.exists()) {
                            if (map.create()) {
                                TeamArea[] teamAreas = map.getTeamAreas();
                                for (TeamArea teamArea : teamAreas) {
                                    teamArea.create();
                                }
                                ChatHandler.sendMessage("§aDie Map wurde erfolgreich erstellt!");
                            } else {
                                ChatHandler.sendMessage("§cDie Map konnte nicht erstellt werden!");
                            }
                        } else {
                            ChatHandler.sendMessage("§cDie Map existiert bereits!");
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> tabs = new ArrayList<>();
        if (args.length == 1) {
            tabs.add("create");
        }
        return tabs;
    }
}
