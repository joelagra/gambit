package gambitMap.commands;

import gambitMap.enums.Area;
import gambitMap.mainFolder.MainApp;
import gambitMap.map.Bank;
import gambitMap.util.ChatHandler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;

public class BankSetupCommand implements CommandExecutor, Listener {
    private final static String AXE_NAME = "§aMagic Staff";
    private final MainApp mainApp;
    private final HashMap<String, Bank> selectedBank;
    private final HashMap<String, Bank> selectedBankLocation;
    private final HashMap<String, Location> firstPosition;
    private final HashMap<String, Integer> animationIDs;

    public BankSetupCommand(MainApp mainApp) {
        this.mainApp = mainApp;
        this.selectedBank = new HashMap<>();
        this.selectedBankLocation = new HashMap<>();
        this.firstPosition = new HashMap<>();
        this.animationIDs = new HashMap<>();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("Gambit.bankSetup")) {
                if (args.length > 1) {
                    if (args[0].equalsIgnoreCase("create")) {
                        if (args.length == 2) {
                            // create the bank
                            createBank(args[1], player);
                        } else {
                            ChatHandler.sendMessage("§cBitte nutze §6/bank <CREATE> <NAME>", player);
                        }
                    } else if (args[0].equalsIgnoreCase("set")) {
                        if (args[1].equalsIgnoreCase("animation")) {
                            if (args.length == 4) {
                                // set the bank
                                setBank(args[2], args[3], player);
                            } else {
                                ChatHandler.sendMessage("§cBitte nutze §6/bank <SET> <ANIMATION> <NAME> <ID>", player);
                            }
                        } else if (args[1].equalsIgnoreCase("area")) {
                            if (args.length == 4) {
                                // set the bank
                                setAreaName(args[2], args[3], player);
                            } else {
                                ChatHandler.sendMessage("§cBitte nutze §6/bank <SET> <AREA> <TEAM_AREA_1, TEAM_AREA_2> <NAME>", player);
                            }
                        } else if (args[1].equalsIgnoreCase("location")) {
                            if (args.length == 3) {
                                // set the bank
                                setAreaBankLocation(args[2], player);
                            } else {
                                ChatHandler.sendMessage("§cBitte nutze §6/bank <SET> <LOCATION> <TEAM_AREA_1, TEAM_AREA_2>", player);
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private void createBank(String name, Player player) {
        // create bank and if successful send message
        Bank bank = new Bank(mainApp, name);
        if (bank.create()) {
            ChatHandler.sendMessage("§aDie Bank wurde erstellt!", player);
        } else {
            ChatHandler.sendMessage("§cDie Bank existiert bereits!", player);
        }
    }

    private void setBank(String name, String animationID, Player player) {
        int id;
        try {
            id = Integer.parseInt(animationID);
        } catch (NumberFormatException e) {
            ChatHandler.sendMessage("§cBitte nutze §6/bank <SET> <NAME> <ID>", player);
            return;
        }

        Bank bank = new Bank(mainApp, name);
        // check if bank exist
        if (bank.exists()) {
            // create item axe
            ItemStack axe = new ItemStack(Material.IRON_AXE);
            ItemMeta axeItemMeta = axe.getItemMeta();
            if (axeItemMeta != null) {
                axeItemMeta.setDisplayName(AXE_NAME);
            }
            axe.setItemMeta(axeItemMeta);

            // add the axe if not in inventory
            if (!player.getInventory().contains(axe)) {
                player.getInventory().addItem(axe);
            }

            // put the portal and the position in arrays
            selectedBank.put(player.getName(), bank);
            animationIDs.put(player.getName(), id);

            ChatHandler.sendMessage("§aJetzt links klicke mit der Axt auf einen Block!", player);
        } else {
            ChatHandler.sendMessage("§cDie Bank existiert nicht!", player);
        }
    }

    private void setAreaName(String areaStr, String name, Player player) {
        Area area = null;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // check if area and enemy spawns is set
        if (area != null) {
            Bank bank = new Bank(mainApp, name, player.getWorld(), area);

            // check if bank exist
            if (bank.exists()) {
                if (bank.saveName()) {
                    ChatHandler.sendMessage("§aDie Bank wurde erfolgreich der Area zugeordnet!", player);
                } else {
                    ChatHandler.sendMessage("§cEs gab ein Problem!", player);
                }
            } else {
                ChatHandler.sendMessage("§cDie Bank existiert nicht!", player);
            }
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/bank <SET> <AREA> <TEAM_AREA_1, TEAM_AREA_2> <NAME>", player);
        }
    }

    private void setAreaBankLocation(String areaStr, Player player) {
        Area area = null;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // check if area and enemy spawns is set
        if (area != null) {
            Bank bank = new Bank(mainApp, player.getWorld(), area);

            // check if bank exist
            if (bank.exists()) {
                // create item axe
                ItemStack axe = new ItemStack(Material.IRON_AXE);
                ItemMeta axeItemMeta = axe.getItemMeta();
                if (axeItemMeta != null) {
                    axeItemMeta.setDisplayName(AXE_NAME);
                }
                axe.setItemMeta(axeItemMeta);

                // add the axe if not in inventory
                if (!player.getInventory().contains(axe)) {
                    player.getInventory().addItem(axe);
                }

                // put the portal and the position in arrays
                selectedBankLocation.put(player.getName(), bank);

                ChatHandler.sendMessage("§aJetzt links klicke mit der Axt auf einen Block!", player);
            } else {
                ChatHandler.sendMessage("§cDie Bank existiert nicht!", player);
            }
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/bank <SET> <LOCATION> <TEAM_AREA_1, TEAM_AREA_2>", player);
        }
    }

    @EventHandler
    public void onAxeHit(PlayerInteractEvent event) {
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
            return;
        }

        Player player = event.getPlayer();
        ItemMeta itemMeta = player.getInventory().getItemInMainHand().getItemMeta();

        if (itemMeta == null) {
            return;
        }

        if (!itemMeta.getDisplayName().equals(AXE_NAME)) {
            return;
        }

        event.setCancelled(true);

        Block block = event.getClickedBlock();
        if (block == null) {
            return;
        }
        Location location = block.getLocation();

        if (selectedBank.containsKey(player.getName()) && !firstPosition.containsKey(player.getName())) {
            firstPosition.put(player.getName(), location);
            ChatHandler.sendMessage("§aDie erste Position wurde erfolgreich gesetzt!", player);
        } else if (selectedBank.containsKey(player.getName()) && firstPosition.containsKey(player.getName())) {
            Location pos1 = firstPosition.remove(player.getName()),
                    pos2 = location.clone();
            Bank bank = selectedBank.remove(player.getName());
            int id = animationIDs.remove(player.getName());

            bank.saveAnimation(pos1, pos2, id);

            ChatHandler.sendMessage("§aDie zweite Position wurde erfolgreich gesetzt!", player);
        } else if (selectedBankLocation.containsKey(player.getName())) {
            Bank bank = selectedBankLocation.remove(player.getName());
            if (bank.saveBankLocation(location)) {
                ChatHandler.sendMessage("§aDie Position wurde erfolgreich gesetzt!", player);
            } else {
                ChatHandler.sendMessage("§cEs kam zu einem Fehler beim Speichern!", player);
            }
        }
    }
}
