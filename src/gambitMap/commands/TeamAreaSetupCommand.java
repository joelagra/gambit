package gambitMap.commands;

import gambitMap.enums.Area;
import gambitMap.enums.InvaderLocations;
import gambitMap.enums.LobbyLocations;
import gambitMap.enums.SpawnLocations;
import gambitMap.mainFolder.MainApp;
import gambitMap.map.TeamArea;
import gambitMap.util.ChatHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class TeamAreaSetupCommand implements CommandExecutor, TabCompleter {
    private final MainApp mainApp;

    public TeamAreaSetupCommand(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("Gambit.teamAreaSetup")) {
                if (args.length == 4) {
                    if (args[0].equalsIgnoreCase("set") && args[1].equalsIgnoreCase("invader")) {
                        setInvaderLocation(args[2], args[3], player);
                    } else if (args[0].equalsIgnoreCase("set") && args[1].equalsIgnoreCase("lobby")) {
                        setLobbyLocation(args[2], args[3], player);
                    } else if (args[0].equalsIgnoreCase("set") && args[1].equalsIgnoreCase("spawn")) {
                        setSpawnLocation(args[2], args[3], player);
                    }
                }
            }
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> tabs = new ArrayList<>();
        if (args.length == 1) {
            tabs.add("set");
        } else if (args.length == 2 && args[0].equalsIgnoreCase("set")) {
            tabs.add("invader");
            tabs.add("lobby");
            tabs.add("spawn");
        } else if (args.length == 3 && args[0].equalsIgnoreCase("set")) {
            tabs.add("team_area_1");
            tabs.add("team_area_2");
        } else if (args.length == 4 && args[0].equalsIgnoreCase("set") && args[1].equalsIgnoreCase("invader")) {
            tabs.add("invader_1");
            tabs.add("invader_2");
            tabs.add("invader_3");
        } else if (args.length == 4 && args[0].equalsIgnoreCase("set") && args[1].equalsIgnoreCase("lobby")) {
            tabs.add("lobby_1");
            tabs.add("lobby_2");
            tabs.add("lobby_3");
            tabs.add("lobby_4");
        } else if (args.length == 4 && args[0].equalsIgnoreCase("set") && args[1].equalsIgnoreCase("spawn")) {
            tabs.add("spawn_1");
            tabs.add("spawn_2");
            tabs.add("spawn_3");
            tabs.add("spawn_4");
        }
        return tabs;
    }

    private void setInvaderLocation(String areaStr, String location, Player player) {
        Area area = null;
        int invaderLocation = 0;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // get the location
        if (location.equalsIgnoreCase(InvaderLocations.INVADER_1.toString())) {
            invaderLocation = 1;
        } else if (location.equalsIgnoreCase(InvaderLocations.INVADER_2.toString())) {
            invaderLocation = 2;
        } else if (location.equalsIgnoreCase(InvaderLocations.INVADER_3.toString())) {
            invaderLocation = 3;
        }

        // check if area and enemy spawns is set
        if (area != null && invaderLocation != 0) {
            TeamArea teamArea = new TeamArea(mainApp, player.getWorld(), area);
            teamArea.load();
            teamArea.getInvaderLocations()[invaderLocation - 1] = player.getLocation();
            teamArea.save();
            ChatHandler.sendMessage("§aDie invader location " + invaderLocation + " wurde erfolgreich gesetzt!", player);
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/bank <SET> <AREA> <TEAM_AREA_1, TEAM_AREA_2> <NAME>", player);
        }
    }

    private void setLobbyLocation(String areaStr, String location, Player player) {
        Area area = null;
        int lobbyLocation = 0;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // get the location
        if (location.equalsIgnoreCase(LobbyLocations.LOBBY_1.toString())) {
            lobbyLocation = 1;
        } else if (location.equalsIgnoreCase(LobbyLocations.LOBBY_2.toString())) {
            lobbyLocation = 2;
        } else if (location.equalsIgnoreCase(LobbyLocations.LOBBY_3.toString())) {
            lobbyLocation = 3;
        } else if (location.equalsIgnoreCase(LobbyLocations.LOBBY_4.toString())) {
            lobbyLocation = 4;
        }

        // check if area and enemy spawns is set
        if (area != null && lobbyLocation != 0) {
            TeamArea teamArea = new TeamArea(mainApp, player.getWorld(), area);
            teamArea.load();
            teamArea.getLobbyLocations()[lobbyLocation - 1] = player.getLocation();
            teamArea.save();
            ChatHandler.sendMessage("§aDie lobby location " + lobbyLocation + " wurde erfolgreich gesetzt!", player);
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/bank <SET> <AREA> <TEAM_AREA_1, TEAM_AREA_2> <NAME>", player);
        }
    }

    private void setSpawnLocation(String areaStr, String location, Player player) {
        Area area = null;
        int spawnLocation = 0;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // get the location
        if (location.equalsIgnoreCase(SpawnLocations.SPAWN_1.toString())) {
            spawnLocation = 1;
        } else if (location.equalsIgnoreCase(SpawnLocations.SPAWN_2.toString())) {
            spawnLocation = 2;
        } else if (location.equalsIgnoreCase(SpawnLocations.SPAWN_3.toString())) {
            spawnLocation = 3;
        } else if (location.equalsIgnoreCase(SpawnLocations.SPAWN_4.toString())) {
            spawnLocation = 4;
        }

        // check if area and enemy spawns is set
        if (area != null && spawnLocation != 0) {
            TeamArea teamArea = new TeamArea(mainApp, player.getWorld(), area);
            teamArea.load();
            teamArea.getPlayerSpawnLocations()[spawnLocation - 1] = player.getLocation();
            teamArea.save();
            ChatHandler.sendMessage("§aDie spawn location " + spawnLocation + " wurde erfolgreich gesetzt!", player);
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/teamArea <SET> <AREA> <TEAM_AREA_1, TEAM_AREA_2> <NAME>", player);
        }
    }
}
