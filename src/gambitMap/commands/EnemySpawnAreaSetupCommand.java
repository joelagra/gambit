package gambitMap.commands;

import gambitMap.enums.Area;
import gambitMap.enums.EnemySpawns;
import gambitMap.mainFolder.MainApp;
import gambitMap.map.EnemySpawnArea;
import gambitMap.util.ChatHandler;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EnemySpawnAreaSetupCommand implements CommandExecutor, Listener, TabCompleter {
    private final static String AXE_NAME = "§aMagic Staff";
    private final MainApp mainApp;
    private final HashMap<String, EnemySpawnArea> selectedEnemySpawnArea;
    private final HashMap<String, Location> firstPosition;

    public EnemySpawnAreaSetupCommand(MainApp mainApp) {
        this.mainApp = mainApp;
        this.selectedEnemySpawnArea = new HashMap<>();
        this.firstPosition = new HashMap<>();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("Gambit.enemySpawnAreaSetup")) {
                if (args.length > 0) {
                    if (args[0].equalsIgnoreCase("create")) {
                        if (args.length == 3) {
                            // create the enemy spawn area
                            createEnemySpawnArea(args[1], args[2], player);
                        } else {
                            ChatHandler.sendMessage("§cBitte nutze §6/enemySpawnArea <CREATE> <TEAM_AREA_1, TEAM_AREA_2> <ENEMY_SPAWN_AREA_1, ENEMY_SPAWN_AREA_2, ENEMY_SPAWN_AREA_3, ENEMY_SPAWN_AREA_MIDDLE>", player);
                        }
                    } else if (args[0].equalsIgnoreCase("set")) {
                        if (args.length == 3) {
                            // set the enemy spawn area
                            setEnemySpawnArea(args[1], args[2], player);
                        } else {
                            ChatHandler.sendMessage("§cBitte nutze §6/enemySpawnArea <SET> <TEAM_AREA_1, TEAM_AREA_2> <ENEMY_SPAWN_AREA_1, ENEMY_SPAWN_AREA_2, ENEMY_SPAWN_AREA_3, ENEMY_SPAWN_AREA_MIDDLE>", player);
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> tabs = new ArrayList<>();
        if (args.length == 1) {
            tabs.add("create");
            tabs.add("set");
        } else if (args.length == 2 && (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("set"))) {
            tabs.add("team_area_1");
            tabs.add("team_area_2");
        } else if (args.length == 3 && (args[0].equalsIgnoreCase("create") || args[0].equalsIgnoreCase("set"))) {
            tabs.add("enemy_spawn_area_1");
            tabs.add("enemy_spawn_area_2");
            tabs.add("enemy_spawn_area_3");
            tabs.add("enemy_spawn_area_middle");
        }
        return tabs;
    }

    private void createEnemySpawnArea(String areaStr, String enemySpawnsStr, Player player){
        Area area = null;
        EnemySpawns enemySpawns = null;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // get the enemy spawn
        if(enemySpawnsStr.equalsIgnoreCase(EnemySpawns.ENEMY_SPAWN_AREA_1.toString())){
            enemySpawns = EnemySpawns.ENEMY_SPAWN_AREA_1;
        } else if(enemySpawnsStr.equalsIgnoreCase(EnemySpawns.ENEMY_SPAWN_AREA_2.toString())){
            enemySpawns = EnemySpawns.ENEMY_SPAWN_AREA_2;
        } else if(enemySpawnsStr.equalsIgnoreCase(EnemySpawns.ENEMY_SPAWN_AREA_3.toString())){
            enemySpawns = EnemySpawns.ENEMY_SPAWN_AREA_3;
        } else if(enemySpawnsStr.equalsIgnoreCase(EnemySpawns.ENEMY_SPAWN_AREA_MIDDLE.toString())){
            enemySpawns = EnemySpawns.ENEMY_SPAWN_AREA_MIDDLE;
        }

        // check if area and enemy spawns is set
        if (area != null && enemySpawns != null) {
            // create enemy spawn area and if successful send message
            EnemySpawnArea enemySpawnArea = new EnemySpawnArea(mainApp, player.getWorld(), area, enemySpawns);
            if (enemySpawnArea.create()) {
                ChatHandler.sendMessage("§aDie EnemySpawnArea wurde erstellt!", player);
            } else {
                ChatHandler.sendMessage("§cDie EnemySpawnArea existiert bereits!", player);
            }
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/enemySpawnArea <CREATE> <TEAM_AREA_1, TEAM_AREA_2> <ENEMY_SPAWN_AREA_1, ENEMY_SPAWN_AREA_2, ENEMY_SPAWN_AREA_3, ENEMY_SPAWN_AREA_MIDDLE>", player);
        }
    }

    private void setEnemySpawnArea(String areaStr, String enemySpawnsStr, Player player){
        Area area = null;
        EnemySpawns enemySpawns = null;

        // get the area
        if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_1.toString())) {
            area = Area.TEAM_AREA_1;
        } else if (areaStr.equalsIgnoreCase(Area.TEAM_AREA_2.toString())) {
            area = Area.TEAM_AREA_2;
        }

        // get the enemy spawn
        if(enemySpawnsStr.equalsIgnoreCase(EnemySpawns.ENEMY_SPAWN_AREA_1.toString())){
            enemySpawns = EnemySpawns.ENEMY_SPAWN_AREA_1;
        } else if(enemySpawnsStr.equalsIgnoreCase(EnemySpawns.ENEMY_SPAWN_AREA_2.toString())){
            enemySpawns = EnemySpawns.ENEMY_SPAWN_AREA_2;
        } else if(enemySpawnsStr.equalsIgnoreCase(EnemySpawns.ENEMY_SPAWN_AREA_3.toString())){
            enemySpawns = EnemySpawns.ENEMY_SPAWN_AREA_3;
        } else if(enemySpawnsStr.equalsIgnoreCase(EnemySpawns.ENEMY_SPAWN_AREA_MIDDLE.toString())){
            enemySpawns = EnemySpawns.ENEMY_SPAWN_AREA_MIDDLE;
        }

        // check if area and enemy spawns is set
        if (area != null && enemySpawns != null) {
            // create enemy spawn area and if successful send message
            EnemySpawnArea enemySpawnArea = new EnemySpawnArea(mainApp, player.getWorld(), area, enemySpawns);
            if (enemySpawnArea.exists()) {
                // create item axe
                ItemStack axe = new ItemStack(Material.IRON_AXE);
                ItemMeta axeItemMeta = axe.getItemMeta();
                if (axeItemMeta != null) {
                    axeItemMeta.setDisplayName(AXE_NAME);
                }
                axe.setItemMeta(axeItemMeta);

                // add the axe if not in inventory
                if (!player.getInventory().contains(axe)) {
                    player.getInventory().addItem(axe);
                }

                // put the portal and the position in arrays
                selectedEnemySpawnArea.put(player.getName(), enemySpawnArea);

                ChatHandler.sendMessage("§aJetzt links klicke mit der Axt auf einen Block!", player);
            } else {
                ChatHandler.sendMessage("§aDie EnemySpawnArea existiert bereits!", player);
            }
        } else {
            ChatHandler.sendMessage("§cBitte nutze §6/enemySpawnArea <SET> <TEAM_AREA_1, TEAM_AREA_2> <ENEMY_SPAWN_AREA_1, ENEMY_SPAWN_AREA_2, ENEMY_SPAWN_AREA_3, ENEMY_SPAWN_AREA_MIDDLE>", player);
        }
    }

    @EventHandler
    public void onAxeHit(PlayerInteractEvent event) {
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
            return;
        }

        Player player = event.getPlayer();
        ItemMeta itemMeta = player.getInventory().getItemInMainHand().getItemMeta();

        if (itemMeta == null) {
            return;
        }

        if (!itemMeta.getDisplayName().equals(AXE_NAME)) {
            return;
        }

        event.setCancelled(true);

        Block block = event.getClickedBlock();
        if(block == null){
            return;
        }
        Location location = block.getLocation();

        if (selectedEnemySpawnArea.containsKey(player.getName()) && !firstPosition.containsKey(player.getName())) {
            firstPosition.put(player.getName(), location);
            ChatHandler.sendMessage("§aDie erste Position wurde erfolgreich gesetzt!", player);
        } else if(selectedEnemySpawnArea.containsKey(player.getName()) && firstPosition.containsKey(player.getName())){
            Location pos1 = firstPosition.remove(player.getName()),
                    pos2 = location.clone();
            EnemySpawnArea enemySpawnArea = selectedEnemySpawnArea.remove(player.getName());

            enemySpawnArea.loopThroughLocations(pos1, pos2);
            enemySpawnArea.save();

            ChatHandler.sendMessage("§aDie zweite Position wurde erfolgreich gesetzt!", player);
        }
    }
}
