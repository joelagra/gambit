package gambitMap.mainFolder;

import gambitMap.commands.*;
import gambitMap.map.Map;
import gambitMap.util.ChatHandler;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class MainApp extends JavaPlugin {
    public static final String PREFIX = "§7[§6Gambit§7] §r";

    @Override
    public void onEnable() {
        ChatHandler.sendConsoleMessage("GambitMap is enabled!");
        init(Bukkit.getPluginManager());

        Map map = new Map(this, Bukkit.getWorlds().get(0));
        map.load();

        map.getTeamAreas()[0].getPortal().activate();
        map.getTeamAreas()[1].getPortal().activate();
    }

    @Override
    public void onDisable() {
        ChatHandler.sendConsoleMessage("GambitMap is disabled!");
    }


    /**
     * register all commands and listeners
     * @param pluginManager for registering all listeners
     */
    private void init(PluginManager pluginManager){
        MapSetupCommand mapSetupCommand = new MapSetupCommand(this);
        TeamAreaSetupCommand teamAreaSetupCommand = new TeamAreaSetupCommand(this);
        PortalSetupCommand portalSetupCommand = new PortalSetupCommand(this);
        EnemySpawnAreaSetupCommand enemySpawnAreaSetupCommand = new EnemySpawnAreaSetupCommand(this);
        BankSetupCommand bankSetupCommand = new BankSetupCommand(this);

        getCommand("map").setExecutor(mapSetupCommand);
        getCommand("teamArea").setExecutor(teamAreaSetupCommand);
        getCommand("portal").setExecutor(portalSetupCommand);
        getCommand("enemySpawnArea").setExecutor(enemySpawnAreaSetupCommand);
        getCommand("bank").setExecutor(bankSetupCommand);

        pluginManager.registerEvents(portalSetupCommand, this);
        pluginManager.registerEvents(enemySpawnAreaSetupCommand, this);
        pluginManager.registerEvents(bankSetupCommand, this);
    }
}
