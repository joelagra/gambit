package gambitMap.map;

import gambitMap.enums.EnemySpawns;
import gambitMap.enums.Area;
import gambitMap.mainFolder.MainApp;
import gambitMap.util.ChatHandler;
import gambitMap.util.FileHandler;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;

public class TeamArea {
    private final MainApp mainApp;
    private final Bank bank;
    private final Portal portal;
    private final World world;
    private final Area teamArea;

    private Location[] lobbyLocations, playerSpawnLocations, invaderLocations;
    private final EnemySpawnArea[] enemySpawnAreas;
    private final EnemySpawnArea middleEnemySpawnAreas;

    public TeamArea(MainApp mainApp, World world, Area teamArea) {
        this.mainApp = mainApp;
        this.world = world;
        this.teamArea = teamArea;

        bank = new Bank(mainApp, world, teamArea);
        portal = new Portal(mainApp, world, teamArea);

        lobbyLocations = new Location[4];
        playerSpawnLocations = new Location[4];
        invaderLocations = new Location[3];

        middleEnemySpawnAreas = new EnemySpawnArea(mainApp, world, teamArea, EnemySpawns.ENEMY_SPAWN_AREA_MIDDLE);
        enemySpawnAreas = new EnemySpawnArea[3];
        enemySpawnAreas[0] = new EnemySpawnArea(mainApp, world, teamArea, EnemySpawns.ENEMY_SPAWN_AREA_1);
        enemySpawnAreas[1] = new EnemySpawnArea(mainApp, world, teamArea, EnemySpawns.ENEMY_SPAWN_AREA_2);
        enemySpawnAreas[2] = new EnemySpawnArea(mainApp, world, teamArea, EnemySpawns.ENEMY_SPAWN_AREA_3);
    }

    public void load(){
        FileConfiguration config = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "teamArea");
        if(config == null){
            ChatHandler.sendConsoleMessage("§cCreate the teamArea first!");
            return;
        }

        List<?> lobbyLocationsList = config.getList("lobbyLocations");
        List<?> playerSpawnLocationsList = config.getList("playerSpawnLocations");
        List<?> invaderLocationsList = config.getList("invaderLocations");

        if(lobbyLocationsList != null){
            lobbyLocations = lobbyLocationsList.toArray(new Location[0]);
        }

        if(playerSpawnLocationsList != null){
            playerSpawnLocations = playerSpawnLocationsList.toArray(new Location[0]);
        }

        if(invaderLocationsList != null){
            invaderLocations = invaderLocationsList.toArray(new Location[0]);
        }

        bank.load();
        portal.load();
        //portal.setInvaderPositions(invaderLocations);
        portal.setSpawnPositions(playerSpawnLocations);
        middleEnemySpawnAreas.load();

        for (EnemySpawnArea enemySpawnArea : enemySpawnAreas) {
            enemySpawnArea.load();
        }
    }

    public void save(){
        FileConfiguration config = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "teamArea");
        if(config == null){
            ChatHandler.sendConsoleMessage("§cCreate the teamArea first!");
            return;
        }

        if(lobbyLocations.length > 0){
            config.set("lobbyLocations", lobbyLocations);
        }
        if(playerSpawnLocations.length > 0){
            config.set("playerSpawnLocations", playerSpawnLocations);
        }
        if(invaderLocations.length > 0){
            config.set("invaderLocations", invaderLocations);
        }
        FileHandler.saveCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "teamArea", config);
    }

    public boolean create(){
        return FileHandler.createCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "teamArea") != null;
    }

    public Bank getBank() {
        return bank;
    }

    public EnemySpawnArea getMiddleEnemySpawnAreas() {
        return middleEnemySpawnAreas;
    }

    public EnemySpawnArea[] getEnemySpawnAreas() {
        return enemySpawnAreas;
    }

    public Location[] getInvaderLocations() {
        return invaderLocations;
    }

    public Location[] getLobbyLocations() {
        return lobbyLocations;
    }

    public Location[] getPlayerSpawnLocations() {
        return playerSpawnLocations;
    }

    public Portal getPortal() {
        return portal;
    }
}
