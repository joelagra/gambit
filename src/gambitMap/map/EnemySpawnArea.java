package gambitMap.map;

import gambitMap.enums.EnemySpawns;
import gambitMap.enums.Area;
import gambitMap.mainFolder.MainApp;
import gambitMap.util.ChatHandler;
import gambitMap.util.FileHandler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class EnemySpawnArea {
    private final MainApp mainApp;
    private List<Vector> enemySpawnLocations;
    private final World world;
    private final Area teamArea;
    private final EnemySpawns enemySpawnArea;

    public EnemySpawnArea(MainApp mainApp, World world, Area teamArea, EnemySpawns enemySpawnArea) {
        this.mainApp = mainApp;
        this.world = world;
        this.teamArea = teamArea;
        this.enemySpawnArea = enemySpawnArea;
        enemySpawnLocations = new ArrayList<>();
    }

    /**
     * Creates a config file of the enemySpawnArea
     * @return if it was created
     */
    public boolean create(){
        return FileHandler.createCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), enemySpawnArea.toString().toLowerCase()) != null;
    }

    /**
     * Checks if the enemySpawnArea exists
     * @return if it exists
     */
    public boolean exists(){
        return FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), enemySpawnArea.toString().toLowerCase()) != null;
    }

    /**
     * save all locations in config
     */
    public void save(){
        // check if the map folder exists
        if(enemySpawnLocations.size() == 0){
            return;
        }

        // check if the map folder exists
        if(!FileHandler.existsFolder(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase())){
            ChatHandler.sendConsoleMessage("§cCreate the map first");
            return;
        }

        // get the config file for the enemy spawn area
        FileConfiguration config = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), enemySpawnArea.toString().toLowerCase());

        // check if the config is set
        if(config == null){
            ChatHandler.sendConsoleMessage("§cDie Config konnte nicht erstellt werden");
            return;
        }

        // set the enemy spawn area in the config and save the config
        config.set("enemySpawnLocations", enemySpawnLocations);
        FileHandler.saveCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), enemySpawnArea.toString(), config);
    }

    /**
     * load all locations from config
     */
    public void load(){
        // get the config file for the enemy spawn area
        FileConfiguration config = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), enemySpawnArea.toString().toLowerCase());

        // check if the config is set and create the config
        if(config == null){
            ChatHandler.sendConsoleMessage("§cDie spawnLocations wurden noch nicht gesetzt!");
            return;
        }

        // get the enemy spawn locations
        enemySpawnLocations = (List<Vector>) config.getList("enemySpawnLocations");
    }

    /**
     * loop through all locations between two points
     * @param pos1 the first position
     * @param pos2 the second position
     */
    public void loopThroughLocations(Location pos1, Location pos2) {
        ArrayList<Vector> spawnPositions = new ArrayList<>();
        Location maxPos = new Location(world, 0, 0, 0),
                minPos= new Location(world, 0, 0, 0);

        if (pos1.getX() > pos2.getX()) {
            maxPos.setX(pos1.getX());
            minPos.setX(pos2.getX());
        } else {
            maxPos.setX(pos2.getX());
            minPos.setX(pos1.getX());
        }

        if (pos1.getY() > pos2.getY()) {
            maxPos.setY(pos1.getY());
            minPos.setY(pos2.getY());
        } else {
            maxPos.setY(pos2.getY());
            minPos.setY(pos1.getY());
        }

        if (pos1.getZ() > pos2.getZ()) {
            maxPos.setZ(pos1.getZ());
            minPos.setZ(pos2.getZ());
        } else {
            maxPos.setZ(pos2.getZ());
            minPos.setZ(pos1.getZ());
        }

        for (double newX = minPos.getX() + 1; newX < maxPos.getX(); newX++) {

            for (double newZ = minPos.getZ() + 1; newZ < maxPos.getZ(); newZ++) {

                boolean setFirst = false;
                boolean setSecond = false;

                for (double newY = minPos.getY(); newY < maxPos.getY(); newY++) {
                    Location location = new Location(world, newX, newY, newZ);
                    Material material = location.getBlock().getType();

                    if(material == Material.BARRIER){
                        break;
                    }

                    if (material != Material.AIR) {
                        setFirst = false;
                        setSecond = false;
                        continue;
                    }

                    if (!setFirst) {
                        setFirst = true;
                    } else if (!setSecond) {
                        setSecond = true;
                        spawnPositions.add(new Vector(location.getX(), location.getY() - 1, location.getZ()));
                    }
                }
            }
        }

        enemySpawnLocations = spawnPositions;
    }

    public List<Vector> getEnemySpawnLocations() {
        return enemySpawnLocations;
    }
}
