package gambitMap.map;

import gambitMap.enums.Area;
import gambitMap.mainFolder.MainApp;
import gambitMap.util.FileHandler;
import org.bukkit.World;

public class Map {
    private final World world;
    private final MainApp mainApp;
    private final TeamArea[] teamAreas;

    // TODO check if map is completed set

    public Map(MainApp mainApp, World world) {
        this.mainApp = mainApp;
        this.world = world;

        teamAreas = new TeamArea[2];
        teamAreas[0] = new TeamArea(mainApp, world, Area.TEAM_AREA_1);
        teamAreas[1] = new TeamArea(mainApp, world, Area.TEAM_AREA_2);
    }

    /**
     * load all parts of this map
     */
    public void load(){
        for (TeamArea teamarea : teamAreas) {
            teamarea.load();
        }

        teamAreas[0].getPortal().setInvaderPositions(teamAreas[1].getInvaderLocations());
        teamAreas[1].getPortal().setInvaderPositions(teamAreas[0].getInvaderLocations());
    }

    /**
     * create the map folder if it does not exist
     * @return if it was created
     */
    public boolean create(){
        return FileHandler.createFolder(mainApp, "map/" + world.getName());
    }

    /**
     * check if the map folder exists
     * @return if it exists
     */
    public boolean exists(){
        return FileHandler.existsFolder(mainApp, "map/" + world.getName());
    }

    public TeamArea[] getTeamAreas() {
        return teamAreas;
    }
}
