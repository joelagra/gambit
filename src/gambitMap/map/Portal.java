package gambitMap.map;

import gambitMap.countdowns.InvaderCountdown;
import gambitMap.enums.Area;
import gambitMap.mainFolder.MainApp;
import gambitMap.util.ChatHandler;
import gambitMap.util.FileHandler;

import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.BoundingBox;
import org.bukkit.Color;

import java.util.Random;

public class Portal {
    private final MainApp mainApp;
    private final World world;
    private final Area teamArea;
    private InvaderCountdown invaderCountdown;
    private Location startPosition, endPosition, middlePosition;
    private Location[] invaderPositions, spawnPositions;
    private int taskID, particleID, seconds;
    private boolean active;

    public Portal(MainApp mainApp, World world, Area teamArea) {
        this.mainApp = mainApp;
        this.world = world;
        this.teamArea = teamArea;
        this.active = false;

        spawnPositions = new Location[4];
        invaderPositions = new Location[3];
    }

    /**
     * Creates a config file of the portal
     * @return if it was created
     */
    public boolean create(){
        return FileHandler.createCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "portal") != null;
    }

    /**
     * Checks if the portal exists
     * @return if it exists
     */
    public boolean exists(){
        return FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "portal") != null;
    }

    /**
     * Checks if the portal is finished
     * @return if the portal is finished
     */
    public boolean finished(){
        // check if every position is set
        boolean invaderPositionsSet = true;
        for (Location invaderPosition : invaderPositions) {
            if (invaderPosition == null) {
                invaderPositionsSet = false;
                break;
            }
        }

        // check if every position is set
        boolean spawnPositionsSet = true;
        for (Location spawnPosition : spawnPositions) {
            if (spawnPosition == null) {
                spawnPositionsSet = false;
                break;
            }
        }

        // return if it is finished
        return startPosition != null && endPosition != null && middlePosition != null && invaderPositionsSet && spawnPositionsSet;
    }

    /**
     * Save the start-, end- and  middlePosition in the config
     */
    public void save(){
        if(startPosition == null && endPosition == null && middlePosition == null){
            return;
        }

        FileConfiguration config = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "portal");
        if(config == null){
            ChatHandler.sendConsoleMessage("§cCreate the portal first!");
            return;
        }

        if(startPosition != null){
            config.set("startPosition", startPosition);
        }
        if(endPosition != null){
            config.set("endPosition", endPosition);
        }
        if(middlePosition != null){
            config.set("middlePosition", middlePosition);
        }

        FileHandler.saveCustomConfig(mainApp,"map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "portal", config);
    }

    /**
     * Load the start-, end- and  middlePosition from the config
     */
    public void load(){
        FileConfiguration config = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "portal");
        if(config == null){
            ChatHandler.sendConsoleMessage("§cCreate the portal first!");
            return;
        }
        startPosition = config.getLocation("startPosition");
        endPosition = config.getLocation("endPosition");
        middlePosition = config.getLocation("middlePosition");
    }

    /**
     * Activate the portal
     */
    public void activate(){
        // check if portal finished
        if(!finished()){
            return;
        }

        // check if portal is active
        if(active){
            return;
        }
        active = true;

        // create the box where entities are checked
        BoundingBox boundingBox = new BoundingBox(startPosition.getX() + 0.5, startPosition.getY(), startPosition.getZ() + 0.5, endPosition.getX() + 0.5, endPosition.getY(), endPosition.getZ() + 0.5);
        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(mainApp, () -> {
            // loop through every entity in area
            for (Entity e : world.getNearbyEntities(boundingBox)) {
                // check if entity is a player
                if (!(e instanceof Player)) {
                    continue;
                }

                // get a random invader position to teleport the player
                e.teleport(invaderPositions[new Random().nextInt(invaderPositions.length)]);

                // start the invader countdown
                invaderCountdown = new InvaderCountdown(mainApp, (Player) e, spawnPositions);
                invaderCountdown.start();

                // deactivate the portal
                deactivate();
                break;
            }
        }, 0, 1);

        // get the middle position and add 0.5
        Location particleMiddle = middlePosition;
        particleMiddle.add(0.5, 0.5, 0.5);

        // start the scheduler to summon the particles
        particleID = Bukkit.getScheduler().scheduleSyncRepeatingTask(mainApp, () -> summonCircle(particleMiddle), 0, 10);
    }

    public void deactivate(){
        // check if portal is not active
        if(!active){
            return;
        }

        // cancel the tasks
        Bukkit.getScheduler().cancelTask(taskID);
        Bukkit.getScheduler().cancelTask(particleID);
    }

    /**
     * animate the portal for specific time
     * @param duration the time how long the animation should last
     * @return if the animation could
     */
    public boolean animate(int duration){
        // check if the positions are set
        if(middlePosition == null || startPosition == null || endPosition == null){
            return false;
        }

        // check if the portal is open
        if(active){
            return false;
        }

        // reset variables
        active = true;
        seconds = duration;

        // get the middle position and add 0.5
        Location particleMiddle = middlePosition;
        particleMiddle.add(0.5, 0.5, 0.5);

        // start scheduler and check if portal can be deactivate
        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(mainApp, () -> {
            if(seconds <= 0){
                deactivate();
            } else{
                seconds--;
            }
        }, 0, 20);

        // get the middle position and add 0.5
        particleID = Bukkit.getScheduler().scheduleSyncRepeatingTask(mainApp, () -> summonCircle(particleMiddle), 0, 10);
        return true;
    }

    /**
     * summon the circle on a location
     * @param location the middle location of the circle
     */
    private void summonCircle(Location location){
        // set variable to check the distance
        boolean xBigger;
        double distanceX = startPosition.getX() - endPosition.getX();
        double distanceZ = startPosition.getZ() - endPosition.getZ();

        // check if the x distance is bigger than the z distance
        if(distanceX < 0 && distanceZ < 0){
            xBigger = distanceX > distanceZ;
        } else if(distanceX < 0 && distanceZ >= 0){
            distanceX *= -1;
            xBigger = distanceX > distanceZ;
        } else if(distanceX >= 0 && distanceZ < 0){
            distanceZ *= -1;
            xBigger = distanceX > distanceZ;
        } else {
            xBigger = distanceX > distanceZ;
        }

        for (double i = 0; i < 3; i += 0.1) {
            // calculate the rgb color
            double current = Math.log(i) * 255;
            int rgb;

            // reverse the rgb color
            if(current <= 0){
                rgb = 255;
            } else if(current > 255){
                rgb = 0;
            } else{
                rgb = 255 - (int) current;
            }

            // summon ring with the rgb color
            summonRing(location, i, Color.fromRGB(rgb, rgb, rgb), xBigger);
        }
    }

    /**
     * summon the ring on a location
     * @param location where the ring will spawn
     * @param size the size of the ring
     * @param color the rgb color of the particles
     * @param xBigger if x is bigger than z
     */
    private void summonRing(Location location, double size, Color color, boolean xBigger) {
        for (int d = 0; d <= ((int) Math.pow(5, size) + 20); d += 1) {
            Location particleLoc = location.clone();
            if(!xBigger){
                particleLoc.setZ(location.getZ() + Math.cos(d) * size);
            }else {
                particleLoc.setX(location.getX() + Math.cos(d) * size);
            }
            particleLoc.setY(location.getY() + Math.sin(d) * size);
            world.spawnParticle(Particle.REDSTONE, particleLoc, 1, 0, 0, 0, 0, new Particle.DustOptions(color, 2), true);
        }
    }

    public void setStartPosition(Location startPosition) {
        this.startPosition = startPosition;
    }

    public void setEndPosition(Location endPosition) {
        this.endPosition = endPosition;
    }

    public void setMiddlePosition(Location middlePosition) {
        this.middlePosition = middlePosition;
    }

    public void setInvaderPositions(Location[] invaderPositions) {
        this.invaderPositions = invaderPositions;
    }

    public void setSpawnPositions(Location[] spawnPositions) {
        this.spawnPositions = spawnPositions;
    }
}