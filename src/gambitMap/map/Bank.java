package gambitMap.map;

import gambitMap.enums.Area;
import gambitMap.mainFolder.MainApp;
import gambitMap.util.ChatHandler;
import gambitMap.util.FileHandler;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    private final MainApp mainApp;
    private final String name;

    private ArrayList<ArrayList<ArrayList<ArrayList<String>>>> animations;
    private World world;
    private Area teamArea;
    private boolean open;
    private Location bankLocation;
    private int taskID, currentFrame = 0;

    public Bank(MainApp mainApp, String name) {
        this.mainApp = mainApp;
        this.name = name;

        animations = new ArrayList<>();
        open = false;
    }

    public Bank(MainApp mainApp, World world, Area area) {
        this.mainApp = mainApp;
        this.world = world;
        this.teamArea = area;
        this.name = getBank();
        this.bankLocation = getLocation();

        animations = new ArrayList<>();
        open = false;
    }

    public Bank(MainApp mainApp, String name, World world, Area area) {
        this.mainApp = mainApp;
        this.name = name;
        this.world = world;
        this.teamArea = area;

        animations = new ArrayList<>();
        open = false;
    }

    /**
     * load all animation from the config
     */
    public void load() {
        // check if the name is set
        if(name == null){
            ChatHandler.sendConsoleMessage("§cThe name is not set!");
            return;
        }

        // get the config file of the bank
        FileConfiguration config = FileHandler.getCustomConfig(mainApp, "banks", name);

        // check if the config is set
        if (config == null) {
            ChatHandler.sendConsoleMessage("§cCreate the bank first!");
            return;
        }

        // get the animations and set the variable
        List<?> list = config.getList("animations");
        animations = (ArrayList<ArrayList<ArrayList<ArrayList<String>>>>) list;
    }

    /**
     * create the config if it does not exist
     * @return if the config was created
     */
    public boolean create() {
        return FileHandler.createCustomConfig(mainApp, "banks", name) != null;
    }

    /**
     * checks if the bank exists
     * @return if the bank exists
     */
    public boolean exists() {
        return FileHandler.getCustomConfig(mainApp, "banks", name) != null;
    }

    /**
     * save the animation in the config
     * @param pos1 the first position
     * @param pos2 the second position
     * @param animID the id of the animation
     * @return if it was saved successful
     */
    public boolean saveAnimation(Location pos1, Location pos2, int animID) {
        // initialize temporary variables
        int i = 0;
        ArrayList<ArrayList<ArrayList<String>>> animation = new ArrayList<>();
        Location maxPos = new Location(pos1.getWorld(), 0, 0, 0),
                minPos = new Location(pos1.getWorld(), 0, 0, 0);

        // check which location has bigger x position
        if (pos1.getX() > pos2.getX()) {
            maxPos.setX(pos1.getX());
            minPos.setX(pos2.getX());
        } else {
            maxPos.setX(pos2.getX());
            minPos.setX(pos1.getX());
        }

        // check which location has bigger y position
        if (pos1.getY() > pos2.getY()) {
            maxPos.setY(pos1.getY());
            minPos.setY(pos2.getY());
        } else {
            maxPos.setY(pos2.getY());
            minPos.setY(pos1.getY());
        }

        // check which location has bigger z position
        if (pos1.getZ() > pos2.getZ()) {
            maxPos.setZ(pos1.getZ());
            minPos.setZ(pos2.getZ());
        } else {
            maxPos.setZ(pos2.getZ());
            minPos.setZ(pos1.getZ());
        }

        // loop through the blocks
        for (double newX = minPos.getX() + 1; newX < maxPos.getX(); newX++, i++) {
            animation.add(new ArrayList<>());
            int j = 0;
            for (double newY = minPos.getY() + 1; newY < maxPos.getY(); newY++, j++) {
                animation.get(i).add(new ArrayList<>());
                for (double newZ = minPos.getZ() + 1; newZ < maxPos.getZ(); newZ++) {
                    // get the blockdata as string of the block on this location
                    Location location = new Location(pos1.getWorld(), newX, newY, newZ);
                    animation.get(i).get(j).add(location.getBlock().getBlockData().getAsString());
                }
            }
        }

        // get the config file of the bank
        FileConfiguration config = FileHandler.getCustomConfig(mainApp, "banks", name);

        // check if the config is set
        if (config == null) {
            ChatHandler.sendConsoleMessage("§cCreate the bank first!");
            return false;
        }

        // get the animations in a temporary variable
        List<?> list = config.getList("animations");
        ArrayList<ArrayList<ArrayList<ArrayList<String>>>> tempAnimations = (ArrayList<ArrayList<ArrayList<ArrayList<String>>>>) list;

        // check if the array is set
        if (tempAnimations == null) {
            tempAnimations = new ArrayList<>();
            tempAnimations.add(animation);
        } else if ((tempAnimations.size() - 1) >= animID) { // check if the animation id is in the array
            tempAnimations.set(animID, animation);
        } else { // add the animation
            tempAnimations.add(animation);
        }

        // set the animation in the config and save it
        config.set("animations", tempAnimations);
        return FileHandler.saveCustomConfig(mainApp, "banks", name, config);
    }

    /**
     * save the bank for the teamArea of the map
     * @return if it was saved successful
     */
    public boolean saveName() {
        // check if the world, the teamArea or the bankLocation are set
        if(world == null || teamArea == null || name == null){
            ChatHandler.sendConsoleMessage("§cThe world, the teamArea or the name are not set!");
            return false;
        }

        // check if the folder exists
        if(!FileHandler.existsFolder(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase())){
            ChatHandler.sendConsoleMessage("§cCreate the map and the teamArea first!");
            return false;
        }

        // get the config file of the bank
        FileConfiguration fileConfiguration = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "bank");

        // check if the config is set
        if(fileConfiguration == null){
            fileConfiguration = FileHandler.createCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "bank");
        }

        // check if the config is set
        if (fileConfiguration == null) {
            ChatHandler.sendConsoleMessage("§The config file can not be created");
            return false;
        }

        // set the bank in the config and save the config
        fileConfiguration.set("bank", name);
        return FileHandler.saveCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "bank", fileConfiguration);
    }

    /**
     * save the bank location for the teamArea of the map
     * @return if it was saved successful
     */
    public boolean saveBankLocation(Location bankLocation) {
        // check if the world, the teamArea or the bankLocation are set
        if(world == null || teamArea == null){
            ChatHandler.sendConsoleMessage("§cThe world, the teamArea are not set!");
            return false;
        }

        // check if the folder exists
        if(!FileHandler.existsFolder(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase())){
            ChatHandler.sendConsoleMessage("§cCreate the map and the teamArea first!");
            return false;
        }

        // get the config file of the bank
        FileConfiguration fileConfiguration = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "bank");

        // check if the config is set
        if(fileConfiguration == null){
            fileConfiguration = FileHandler.createCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "bank");
        }

        // check if the config is set
        if (fileConfiguration == null) {
            ChatHandler.sendConsoleMessage("§The config file can not be created");
            return false;
        }

        // set the bank location in the config and save the config
        fileConfiguration.set("location", bankLocation);
        return FileHandler.saveCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "bank", fileConfiguration);
    }

    /**
     * opens or closes the bank
     * @return if it was successful animated
     */
    public boolean animate() {
        if (open) {
            return close();
        } else {
            return open();
        }
    }

    /**
     * opens the bank
     * @return if it was successful opened
     */
    public boolean open() {
        // check if the bank is open
        if (open){
            ChatHandler.sendConsoleMessage("§cThe bank is already open!");
            return false;
        }

        // check if the animation and bankLocation is set
        if (animations.size() <= 0 || bankLocation == null) {
            ChatHandler.sendConsoleMessage("§cThe animation or the bankLocation is not!");
            return false;
        }

        // reset variable to open the bank
        open = true;
        currentFrame = 0;

        // start the scheduler to animate the bank
        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(mainApp, () -> {
            double newX = bankLocation.getX();
            for (int i = 0; i < animations.get(currentFrame).size(); i++, newX++) {
                double newY = bankLocation.getY();
                for (int j = 0; j < animations.get(currentFrame).get(i).size(); j++, newY++) {
                    double newZ = bankLocation.getZ();
                    for (int k = 0; k < animations.get(currentFrame).get(i).get(j).size(); k++, newZ++) {
                        // get the new location
                        Location newLocation = new Location(bankLocation.getWorld(), newX, newY, newZ);

                        // create blockdata of string
                        BlockData blockData = Bukkit.getServer().createBlockData(animations.get(currentFrame).get(i).get(j).get(k));

                        // check if blockdata matches the current
                        if (newLocation.getBlock().getBlockData().matches(blockData)) {
                            continue;
                        }

                        // set the type and the blockdata of the block
                        newLocation.getBlock().setType(blockData.getMaterial());
                        newLocation.getBlock().setBlockData(blockData);
                    }
                }
            }

            // check if it is finished
            if (currentFrame >= (animations.size() - 1)) {
                stop();
            } else {
                currentFrame++;
            }
        }, 0, 5);

        // return it was successful
        return true;
    }

    /**
     * closes the bank
     * @return if it was successful closed
     */
    public boolean close() {
        // check if the bank is open
        if (!open){
            ChatHandler.sendConsoleMessage("§cThe bank is already closed!");
            return false;
        }

        // check if the animation and bankLocation is set
        if (animations.size() <= 0 || bankLocation == null) {
            ChatHandler.sendConsoleMessage("§cThe animation or the bankLocation is not!");
            return false;
        }

        // reset variable to close the bank
        open = false;
        currentFrame = animations.size() - 1;

        // start the scheduler to animate the bank
        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(mainApp, () -> {
            double newX = bankLocation.getX();
            for (int i = 0; i < animations.get(currentFrame).size(); i++, newX++) {
                double newY = bankLocation.getY();
                for (int j = 0; j < animations.get(currentFrame).get(i).size(); j++, newY++) {
                    double newZ = bankLocation.getZ();
                    for (int k = 0; k < animations.get(currentFrame).get(i).get(j).size(); k++, newZ++) {
                        // get the new location
                        Location newLocation = new Location(bankLocation.getWorld(), newX, newY, newZ);

                        // create blockdata of string
                        BlockData blockData = Bukkit.getServer().createBlockData(animations.get(currentFrame).get(i).get(j).get(k));

                        // check if blockdata matches the current
                        if (newLocation.getBlock().getBlockData().matches(blockData)) {
                            continue;
                        }

                        // set the type and the blockdata of the block
                        newLocation.getBlock().setType(blockData.getMaterial());
                        newLocation.getBlock().setBlockData(blockData);
                    }
                }
            }

            // check if it is finished
            if (currentFrame <= 0) {
                stop();
            } else {
                currentFrame--;
            }
        }, 0, 5);

        // return it was successful
        return true;
    }

    /**
     * stops the current scheduler
     */
    private void stop() {
        Bukkit.getScheduler().cancelTask(taskID);
    }

    /**
     * get the name of the selected bank of the teamArea from the config
     * @return the name of the selected bank
     */
    private String getBank(){
        // check if world and teamArea are set
        if(world == null || teamArea == null){
            ChatHandler.sendConsoleMessage("§cThe world or the teamArea is not set!");
            return null;
        }

        // get the config file of the bank
        FileConfiguration fileConfiguration = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "bank");

        // check if the config is set
        if(fileConfiguration == null){
            ChatHandler.sendConsoleMessage("§cThe config does not exist!");
            return null;
        }

        // return the name of the bank
        return fileConfiguration.getString("bank");
    }

    /**
     * get the bank location of the selected bank of the teamArea from the config
     * @return the bank location of the selected bank
     */
    private Location getLocation(){
        // check if world and teamArea are set
        if(world == null || teamArea == null){
            ChatHandler.sendConsoleMessage("§cThe world or the teamArea is not set!");
            return null;
        }

        // get the config file of the bank
        FileConfiguration fileConfiguration = FileHandler.getCustomConfig(mainApp, "map/" + world.getName() + "/" + teamArea.toString().toLowerCase(), "bank");

        // check if the config is set
        if(fileConfiguration == null){
            ChatHandler.sendConsoleMessage("§cThe config does not exist!");
            return null;
        }

        // return the name of the bank
        return fileConfiguration.getLocation("location");
    }

    public ArrayList<ArrayList<ArrayList<ArrayList<String>>>> getAnimations() {
        return animations;
    }
}
