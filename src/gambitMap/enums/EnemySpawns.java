package gambitMap.enums;

public enum EnemySpawns {
    ENEMY_SPAWN_AREA_1,
    ENEMY_SPAWN_AREA_2,
    ENEMY_SPAWN_AREA_3,
    ENEMY_SPAWN_AREA_MIDDLE
}
