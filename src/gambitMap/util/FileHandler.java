package gambitMap.util;

import gambitMap.mainFolder.MainApp;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileHandler {
    /**
     * Saves the custom config if it does not exist
     * @param mainApp The plugin
     * @param path The path for the file
     * @param fileName The name for the file
     * @param customConfig The config that will be saved
     * @return if it was saved
     */
    public static boolean saveCustomConfig(MainApp mainApp, String path, String fileName, FileConfiguration customConfig) {
        // check if plugin folder is set
        if (!mainApp.getDataFolder().exists()) {
            return false;
        }

        // check if path of file is set
        File filePath = new File(mainApp.getDataFolder() + "/" + path);
        if(!filePath.exists()){
            return false;
        }

        // check if file is set
        File customConfigFile = new File(filePath, fileName + ".yml");
        if (!customConfigFile.exists()) {
            return false;
        }

        // save config
        try {
            customConfig.save(customConfigFile);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Creates the custom config if it does not exist
     * @param mainApp The plugin
     * @param path The path for the file
     * @param fileName The name for the file
     * @return the config if it was created
     */
    public static FileConfiguration createCustomConfig(MainApp mainApp, String path, String fileName) {
        // check if plugin folder is set and create folder
        if (!mainApp.getDataFolder().exists()) {
            mainApp.getDataFolder().mkdir();
        }

        // check if path of file is set and create path
        File filePath = new File(mainApp.getDataFolder() + "/" + path);
        if(!filePath.exists()) {
            filePath.mkdirs();
        }

        // check if file is set and return
        File customConfigFile = new File(filePath, fileName + ".yml");
        if (customConfigFile.exists()) {
            return null;
        }

        // create the file
        try {
            if(!customConfigFile.createNewFile()){
                return null;
            } else{
                // load the config and return it
                FileConfiguration customConfig = new YamlConfiguration();
                customConfig.load(customConfigFile);
                return customConfig;
            }
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get the custom config if it exists
     * @param mainApp The plugin
     * @param path The path for the file
     * @param fileName The name for the file
     * @return the config if it exists
     */
    public static FileConfiguration getCustomConfig(MainApp mainApp, String path,  String fileName) {
        // check if plugin folder is set
        if (!mainApp.getDataFolder().exists()) {
            return null;
        }

        // check if path of file is set
        File filePath = new File(mainApp.getDataFolder() + "/" + path);
        if(!filePath.exists()){
            return null;
        }

        // check if file is set
        File customConfigFile = new File(filePath, fileName + ".yml");
        if (!customConfigFile.exists()) {
            return null;
        }

        // load the config and return it
        FileConfiguration customConfig = new YamlConfiguration();
        try {
            customConfig.load(customConfigFile);
            return customConfig;
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param mainApp The plugin
     * @param path The path for the folder
     * @return if the folder was created
     */
    public static boolean createFolder(MainApp mainApp, String path){
        // check if plugin folder is set and create folder
        if (!mainApp.getDataFolder().exists()) {
            mainApp.getDataFolder().mkdir();
        }

        // check if path is set and create path
        File folderPath = new File(mainApp.getDataFolder() + "/" + path);
        if(!folderPath.exists()) {
            folderPath.mkdirs();
            return true;
        }
        return false;
    }

    /**
     * check if the folder exists
     * @param mainApp The plugin
     * @param path The path for the folder
     * @return if the folder exist
     */
    public static boolean existsFolder(MainApp mainApp, String path){
        // check if plugin folder is set and create folder
        if (!mainApp.getDataFolder().exists()) {
            mainApp.getDataFolder().mkdir();
        }

        // return if folder exists
        File folderPath = new File(mainApp.getDataFolder() + "/" + path);
        return folderPath.exists();
    }
}
