package gambitMap.util;

import gambitMap.mainFolder.MainApp;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ChatHandler {
    /**
     * sends a message in the console
     * @param message The message for the console
     */
    public static void sendConsoleMessage(String message){
        Bukkit.getConsoleSender().sendMessage(MainApp.PREFIX + message);
    }

    /**
     * sends a message to all players
     * @param message The message for all players
     */
    public static void sendMessage(String message){
        Bukkit.broadcastMessage(MainApp.PREFIX + message);
    }

    /**
     * sends a message to a player array
     * @param message The message for the players
     * @param players The array of players
     */
    public static void sendMessage(String message, Player... players){
        for (Player player : players) {
            player.sendMessage(MainApp.PREFIX + message);
        }
    }
}
